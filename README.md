# fusilli.io-concatter

## Debug your project

First step is to expose all enviroment variables:

```bash
export input_edges='{"input_edges":[{"label":"String","host":"String","port":"Int","group_id":"String","topic":"String","user":"String","password":"String"}]}'

export output_edges='{"output_edges":[{"label":"String","host":"String","port":"Int","group_id":"String","topic":"String","user":"String","password":"String"}]}'

export job_configs='{"job_configs":[{"input_attrs":["PROVINCIA","COMUNE"],"output_attrs":["RESIDENZA"],"config":{"spacing":" - "}}]}'
```


To test locally the project in debug mode run the following command inside the project folder:

```bash
sbt -jvm-debug 5005 run
```
