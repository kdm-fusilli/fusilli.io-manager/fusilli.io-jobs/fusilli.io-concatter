package io.fusilli.concatter.classes

import play.api.libs.json.{Format, JsValue, Json}

object Classes {

  case class FusilliQueue(
                           host: String,
                           port: Int,
                           group_id: String,
                           topic: String,
                           user: String,
                           password: String
                         )

  object FusilliQueue {
    implicit val format: Format[FusilliQueue] = Json.format[FusilliQueue]
  }

  case class ConcatConfig(
                           input_attrs: List[String],
                           output_attrs: List[String],
                           config: SubConfig
                         )

  object ConcatConfig {
    implicit val format: Format[ConcatConfig] = Json.format[ConcatConfig]
  }

  case class SubConfig(
                        spacing: String
                      )

  object SubConfig {
    implicit val format: Format[SubConfig] = Json.format[SubConfig]
  }

  case class JsonMessage(
                          header: Header,
                          data: JsValue,
                        )

  object JsonMessage {
    implicit val format: Format[JsonMessage] = Json.format[JsonMessage]
  }

  case class Header(
                     status: String
                   )

  object Header {
    implicit val format: Format[Header] = Json.format[Header]
  }


}
