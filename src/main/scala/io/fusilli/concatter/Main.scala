package io.fusilli.concatter

import io.fusilli.concatter.classes.Classes.ConcatConfig
import io.fusilli.concatter.config.ConfigApp
import io.fusilli.concatter.services.Concatter
import org.slf4j.{Logger, LoggerFactory}

object Main extends App {
  private final val log: Logger = LoggerFactory.getLogger(this.getClass.getName)

  val inputQueue = ConfigApp.getInputConfigJson()
  val outputQueue = ConfigApp.getOutputConfigJson()
  val configs = ConfigApp.getJobConfigJson()

  val configuration: List[ConcatConfig] = (configs.value.map(_.as[ConcatConfig])).toList

  Concatter.concat(configuration, inputQueue, outputQueue)

}