package io.fusilli.concatter.services

import io.fusilli.concatter.classes.Classes.ConcatConfig
import io.fusilli.concatter.config.Constants.job_config
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.json.JSONObject

object LoggingServices {

  def sendLog(cfg: List[ConcatConfig], status: String, logProducer: KafkaProducer[String, String], percentage: String, message: String): Unit = {

    val logQueue = "fusilli_pipelines_logger"
//    val cfgString = System.getenv(job_config.toString)
//    val jsonCfg: JSONObject = new JSONObject(cfgString)
    val instanceId: String = System.getenv("""instance_id""")
    val pipelineId: String = System.getenv("""pipeline_id""")
    val policiesId: String = System.getenv("""policies_id""")
    val version: String = System.getenv("""version""")
//    val jobConfigurationId: String = jsonCfg.get("job_configuration_id").toString
    val jobConfigurationId: String =  System.getenv("""job_configuration_id""")

    val jsonLogger: JSONObject = new JSONObject()
    jsonLogger.put("instance_id", instanceId)
    jsonLogger.put("pipeline_id", pipelineId)
    jsonLogger.put("policies_id", policiesId)
    jsonLogger.put("version", version)
    jsonLogger.put("job_configuration_id", jobConfigurationId)
    jsonLogger.put("status", status)
    jsonLogger.put("message", message)
    jsonLogger.put("timestamp", System.currentTimeMillis().toString)
    jsonLogger.put("percentage", percentage)

    logProducer.send(new ProducerRecord[String, String](logQueue, "keyName", jsonLogger.toString))
  }
}