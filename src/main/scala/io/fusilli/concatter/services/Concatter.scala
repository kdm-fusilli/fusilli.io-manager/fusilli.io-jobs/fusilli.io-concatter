package io.fusilli.concatter.services

import io.fusilli.concatter.classes.Classes.{ConcatConfig, FusilliQueue, JsonMessage}
import io.fusilli.concatter.services.KafkaServices.{getKafkaConsumer, getKafkaLogProducer, getKafkaProducer}
import io.fusilli.concatter.services.LoggingServices.sendLog
import kafka.common.KafkaException
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.config.ConfigException
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsObject, JsString, Json}

import java.io.FileNotFoundException
import java.time.Duration
import java.util
import scala.jdk.CollectionConverters.IterableHasAsScala

object Concatter {
  private final val log: Logger = LoggerFactory.getLogger("Concatter")

  def concat(configs: List[ConcatConfig], inputQueue: FusilliQueue, outputQueue: FusilliQueue): Unit = {
    log.info("Executing job...")
    val kafkaLogProducer = getKafkaLogProducer(outputQueue.host, outputQueue.port)
    sendLog(configs, "START", kafkaLogProducer, "0", "")
    try {

      val consumer: KafkaConsumer[String, String] = getKafkaConsumer(inputQueue.host, inputQueue.port, inputQueue.group_id)

      val producer: KafkaProducer[String, String] = getKafkaProducer(outputQueue.host, outputQueue.port)

      consumer.subscribe(util.Arrays.asList(inputQueue.topic))

      var cycle = true

      while (cycle) {
        val record = consumer.poll(Duration.ofMillis(1000)).asScala //this is used to read a line in the queue
        for (data <- record.iterator) {
          log.info("Row retrieved.")
          var jsonMessage = Json.parse(data.value()).as[JsonMessage]
          var newJsonMessage = ""

          if (!jsonMessage.header.status.equals("end")) {
            log.info("Executing job...")
            val row = concatFun(jsonMessage.data.as[JsObject], configs)
            newJsonMessage = Json.toJson(new JsonMessage(jsonMessage.header, row)).toString
          } else {
            newJsonMessage = Json.toJson(jsonMessage).toString
            log.info("Executing end...")
            cycle = false
          }
          log.info("Job Executed. Sending row to Kafka...")
          producer.send(new ProducerRecord[String, String](outputQueue.topic, "keyName", newJsonMessage))
        }
      }

      sendLog(configs, "END", kafkaLogProducer, "100", "")
    } catch {
      case f: FileNotFoundException => log.error("Unable to find file.")
      case k: KafkaException => log.error("Unable to connect to Kafka.")
      case c: ConfigException => log.error("Unable to retrieve config for Kafka")
    }
  }

  def concatFun(doc: JsObject, jobConfigs: List[ConcatConfig]): JsObject = {
    var newDoc = doc
    for (j <- jobConfigs) {
      var newVal = List[String]()
      for (attr <- j.input_attrs) {
        newVal = newVal :+ (newDoc \ attr).as[String]
        newDoc = newDoc - attr
      }
      newDoc = newDoc + (j.output_attrs(0) -> JsString(newVal.mkString(j.config.spacing)))
    }
    newDoc
  }
}
