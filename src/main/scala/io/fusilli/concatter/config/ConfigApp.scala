package io.fusilli.concatter.config

import io.fusilli.concatter.classes.Classes.FusilliQueue
import io.fusilli.concatter.config.Constants._
import play.api.libs.json._

object ConfigApp {


  def getKafkaLogHost(): JsValue = {
    val input_edges_str = System.getenv(log_queue.toString)
    val json: JsValue = Json.parse(input_edges_str)
    //TODO: return host:port for log connection
    json
  }

  def getInputConfigJson(): FusilliQueue = {
    val input_edges_str = System.getenv(input_edges.toString)
    val json: FusilliQueue = (Json.parse(input_edges_str) \ "input_edges").as[JsArray].apply(0).as[FusilliQueue]
    //println("\n\n\n" + json)
    json
  }


  def getOutputConfigJson(): FusilliQueue = {
    val output_edges_str = System.getenv(output_edges.toString)
    val json: FusilliQueue = (Json.parse(output_edges_str) \ "output_edges").as[JsArray].apply(0).as[FusilliQueue]
    //println("\n\n\n" + json)
    json
  }


  def getJobConfigJson(): JsArray = {
    val job_configs_str = System.getenv(job_config.toString)
    val json: JsArray = (Json.parse(job_configs_str)).as[JsArray]
    //println("\n\n\n" + json)
    json
  }

}
