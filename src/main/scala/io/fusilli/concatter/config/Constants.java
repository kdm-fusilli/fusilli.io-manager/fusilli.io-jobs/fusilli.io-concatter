package io.fusilli.concatter.config;

public enum Constants {

    input_edges,
    output_edges,
    job_config,
    log_queue
}
