import Library._

name := "fusilli.io-concatter"

version := "0.1"

scalaVersion := "2.13.5"

//def dockerSettings = Seq(
//  dockerBaseImage := "adoptopenjdk/openjdk8",
//  dockerRepository := sys.props.get("docker.registry")
//)

libraryDependencies ++= Seq(
  Slf4j,
  AkkaStream,
  akkaStreamAlpakkaSlick,
  KafkaClient,
  PlayLib,
  OrgJson
)

//javaOptions in Universal ++= Seq(
//  "-Dpidfile.path=/dev/null"
//)
